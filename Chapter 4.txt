Metode Pengembangan Sistem 
Pengembangan sistem yang kami buat menggunakan metode waterfall
Tahapan Pengembangan Sistem Konseling Online
1. Analisis Kebutuhan Perangkat Lunak
Pengumpulan kebutuhan untuk menspesifikasikan kebutuhan perangkat lunak sehingga dapat dipahami kebutuhan dari user.
Menentukan bahasa pemrograman yang akan digunakan.
2. Desain
Desain pembuatan program perangkat lunak termasuk struktur data, arsitektur perangkat lunak,
representasi antar muka dan prosedur pengkodean.
3. Pembuatan Kode Program
Hasil tahap ini adalahprogram komputer sesuai dengan desain yang telah dibuat pada tahap desain.
4. Pengujian
Pengujian fokus pada perangkat lunak dari segi logik dan fungsional serta memastikan bahwa semua bagian sudah 
diuji sehingga keluaran yang dihasilkan sesuai dengan
yang diinginkan.
5. Pendukung atau Pemeliharaan
Dikarenakan adanya perubahan ketika sudah dikirimkan ke
user. Perubahan dapat terjadi karena adanya kesalahan
yang muncul dan tidak terdeteksi saat pengujian.